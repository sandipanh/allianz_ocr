
import cv2
import os
import time
import requests
import json
import logging
from pathlib import Path
import re

import shutil
from pdf2image import convert_from_path
from MakeTreeDir import MAKETREEDIR
from PyPDF2 import PdfFileReader



import shutil
import warnings
# handling warning as error
warnings.simplefilter("error" ,category='DecompressionBombWarning')




import numpy as np
import math
from collections import defaultdict



def azure_api( image, subscription_key, url="https://centralus.api.cognitive.microsoft.com/vision/v2.0/"):

		"""function to call the azure api, pass the image and returns the json
		file and word dictionary

		:param type image: input image
		:param type subscription_key: Enter the subscription_key
		:param type url: enter the url of api call
		:return: the json out from azure and word dictionary of the same.
		:rtype: (json,dict)

		"""
		### converting image to bytes
        
		res,string=cv2.imencode('.jpg',image)
		image_data=string.tobytes()
		### setting requests params
		ocr_url = url + "recognizeText"
		headers = {'Content-Type':'application/octet-stream','Ocp-Apim-Subscription-Key': subscription_key}
		params  = {'mode':'Printed'}
		t=time.time()

		### posting request
		response = requests.post(ocr_url, headers=headers, params=params, data=image_data,verify=False)
		send_status=response.status_code
		print(send_status) 
		if send_status==202:
			operation_location=response.headers['Operation-Location']
			operation_location_id=operation_location[operation_location.find('textOperations/')+15:]
			receive_status='Running'
			### wait till output returns
			while receive_status=='Running':
				### get output
				headers = {'Ocp-Apim-Subscription-Key': subscription_key}
				params  = {'operationId':operation_location_id}
				response = requests.get(operation_location, headers=headers, params=params,verify=False)
				result=response.json()
				receive_status=result['status']
				if (time.time()-t)>300:
					logging.debug('azure api taking long time')
					result= {'status':'false'}
					break
		else:
			logging.debug('azure api failed')
			result= {'status':'false'}
		###converting json to word dict
# 		word_dict=azure_to_worddict(result)
		return result


# def azure_api( image, subscription_key, url="https://centralus.api.cognitive.microsoft.com/vision/v2.0/"):

# 		"""function to call the azure api, pass the image and returns the json
# 		file and word dictionary

# 		:param type image: input image
# 		:param type subscription_key: Enter the subscription_key
# 		:param type url: enter the url of api call
# 		:return: the json out from azure and word dictionary of the same.
# 		:rtype: (json,dict)

# 		"""
# 		### converting image to bytes
		
# 		res,string=cv2.imencode('.jpg',image)
# 		image_data=string.tobytes()
# 		### setting requests params
# 		ocr_url = url + "recognizeText"
# 		headers = {'Content-Type':'application/octet-stream','Ocp-Apim-Subscription-Key': subscription_key}
# 		params  = {'mode':'Printed'}
# 		t=time.time()

# 		### posting request
# 		response = requests.post(ocr_url, headers=headers, params=params, data=image_data,verify=False)
# 		send_status=response.status_code
# 		print(send_status) 
# 		if send_status==202:
# 			operation_location=response.headers['Operation-Location']
# 			operation_location_id=operation_location[operation_location.find('textOperations/')+15:]
# 			receive_status='Running'
# 			### wait till output returns
# 			while receive_status=='Running':
# 				### get output
# 				headers = {'Ocp-Apim-Subscription-Key': subscription_key}
# 				params  = {'operationId':operation_location_id}
# 				response = requests.get(operation_location, headers=headers, params=params,verify=False)
# 				result=response.json()
# 				receive_status=result['status']
# 				if (time.time()-t)>300:
# 					logging.debug('azure api taking long time')
# 					result= {'status':'false'}
# 					break
# 		else:
# 			logging.debug('azure api failed')
# 			result= {'status':'false'}
# 		###converting json to word dict
# # 		word_dict=azure_to_worddict(result)
# 		return result


# given u have converted pdf to set of images in a folder 

# def images2text(folder_path=None, output_folder_path=None):
#     image_paths = [os.path.abspath(os.path.join(folder_path, p)) for p in os.listdir(folder_path) if p.endswith('jpg') ]
#     print(image_paths)
#     if output_folder_path != None:
#         output_path=os.path.abspath(output_folder_path)
#     else:
#         output_path=os.path.abspath(os.path.join(folder_path,"output"))
#     os.makedirs(output_path, exist_ok=True)
		
#     print(output_path)
#     for i in image_paths:
#         img = cv2.imread(i)
		
		
#         # save dictionary as json
		
#         file_path =  os.path.join(output_path,i[i.rindex('/')+1:][:-3]+'json' )
#         print(file_path)
		
#         my_file = Path(file_path)

#         if not my_file.is_file():
#             f =open(file_path, 'w')
#             try:
#                 res=azure_api(img,os.environ['COMPUTER_VISION_SUBSCRIPTION_KEY'], os.environ['COMPUTER_VISION_ENDPOINT']+'vision/v2.0/')
			
#                 js= json.dumps(res)

#                 f.write(js)
#             except Exception as e:
#                 print(e)
#             finally:
#                 f.close()

# ['pilot name', 'age']

def keyword_val(keywords, json): # keyword = ['fgdf','sgs']
	result=[]
	if json['status'] == "Succeeded":
		for line_num in range(len(json['recognitionResult']['lines'])):
			line = json['recognitionResult']['lines'][line_num]
			line_text  = line['text'].lower()
			
			for key in keywords:
				g =  re.findall(r'\b' + key.lower() + r'\b', line_text)
				if len(g) > 0:
					result.append(line_text)
	return result if len(result)>0 else None

## return the line if matched keyword is found
# Find keyword in pdf


def keyword_pdf_val(keywords, images_path):
	out_folder_path = os.path.abspath(os.path.join(images_path, "output"))
	pdf_val=[]
	
	out_folder_path = Path(out_folder_path)
	files_in_outpath = out_folder_path.iterdir()
	for page in (files_in_outpath):

		with open(page) as f:
			js=json.load(f)
			result= keyword_val(keywords, js)
			if result !=None:
				print(page.name)
				pdf_val.append({int(page.name[page.name.rindex('_')+1: page.name.rindex('.json')]):result})
				print(pdf_val)
	return pdf_val if len(pdf_val) > 0 else None

### CONVERT PDF TO IMAGES


def pdf2images(input_path_pdf, output_image_folder, reject_folder='./reject_folder', thres=300, rerun_thres=150):
	logging.info('converting pdf2image {}'.format(input_path_pdf))
	
	directory= MAKETREEDIR()
	try:
		directory.makedir(output_image_folder)
		### calling pdf2image to convert pdf to images
		pages = convert_from_path(input_path_pdf,thres)
		
		file_name = Path(input_path_pdf).name[:-4]
		pdf = PdfFileReader(open(input_path_pdf,'rb'))
		l = pdf.getNumPages()
		if l != len(pages):
			raise ValueError("Error: Number of pages in pdf doesn't match number of pages")
		
		### saving pages in output_image_folder
		
		for page in pages:
			
			image_path = os.path.join(output_image_folder, file_name+'_{}.jpg'.format(pages.index(page)+1))
		
			if not os.path.isfile(image_path):
				page.save(image_path, 'JPEG')
		
	except Exception as e:
		logging.debug('pdf2image error: {}'.format(e))
		print(f'\n ********************OCR may fail here for pdf {input_path_pdf}:{e} \n ')
		print(f"**************************running with thres={rerun_thres} \n")
		pdf2images(input_path_pdf,output_image_folder,thres=rerun_thres)
		directory.makedir(reject_folder)
		shutil.copy(input_path_pdf, reject_folder)   # add the pdf to the reject folder 
		return e
	
	
	return True
		
## Run this on all the brokers 
## Need to run this before any other algorithm can work
def dir_to_images(input_folder,output_folder, reject_folder='./reject_folder',thres=300):
	for root,dirname, files in os.walk(input_folder):
		for item in files:
			print(item)
			filepath = str(os.path.join(root,item))
			output_image_path = os.path.join(output_folder,os.path.relpath(filepath,input_folder)[:-4])
			pdf2images(filepath,output_image_path,reject_folder=reject_folder, thres=thres, rerun_thres=150)
			
			
			
### CONVERT IMAGES TO JSON
		
def images2json(images_folder, subscription_key, url, output=None, rerun=2, reject_folder=None):
	'''
	Params:
	:images_folder: Path to images folder
	:subscription_key: Azure subscription key
	:url: azure endpoint url
	:output: Path to json output folder . Keep it as None unless u are experimenting with Azure api
	:rerun: Max number of rerun if Azure ocr fails to create json output for all the images 
	'''
	images_folder =  Path(images_folder)
#     image_paths = [os.path.abspath(os.path.join(images_folder, p)) for p in os.listdir(images_folder) if p.endswith('jpg') ]
	image_paths = [str(i) for i in images_folder.iterdir() if i.is_file()]
#     print(image_paths)
	run_count=0
	if output is None:
		output = os.path.abspath(os.path.join(images_folder,'output'))
		
	directory = MAKETREEDIR()
	directory.makedir(output)
	
	if reject_folder is None:
		reject_folder = os.path.join(images_folder,'reject_folder')
	while(run_count <= rerun):
		loop_image_paths = image_paths[:]
		if len(loop_image_paths) == 0:
			break
			
		indices=[]
		for i in range(len(loop_image_paths)):
			
			outfile_name= Path(os.path.join(output, Path(loop_image_paths[i]).name[:-4]+'.json'))
			if outfile_name.is_file():
				indices.append(i)
				
			else:
				try:
					img = cv2.imread(loop_image_paths[i])
					print(f"Azure api hit with {loop_image_paths[i]}")
					res=azure_api(img,subscription_key,url)
					if res['status'] != "Succeeded":
						print(res['status'])
						continue

					# save result 

					f= open(outfile_name,'w')

					js =  json.dumps(res)
					print("saving json")
					f.write(js)
					f.close()
					
					# pop image from image_path
					indices.append(i)

				except Exception as e: 
					# log and continue
					logging.debug('ocr error: {}'.format(e))
					print("error occured")
					directory.makedir(reject_folder)
					shutil.copy(image_paths[i], reject_folder)
			
				
		
				
			image_paths = [i for j, i in enumerate(image_paths) if j not in indices]
			
				
			
		run_count=run_count+1
				
				
	# if fails after reruns put images in the reject folder 
	if len(image_paths) > 0 :
		directory.makedir(reject_folder)
		for path in image_paths:
			print(f'moving {path} to reject folder')
			shutil.copy(path, reject_folder)


		
## Run this on all the subfolders
def dir_to_json(input_dir, subscription_key, url, rerun=10):
	# go through dir and find image directory . 
	
	for root, dirname, files in os.walk(input_dir):
#         print(root,"***********************root")
		if not root.endswith(('reject_folder','underlined_images','output_underline','output_checkbox','checkbox_images','output_tables')):
			if len(files) > 0: 
#                 print(files,"===========files")
				if files[0].endswith(('.jpg', '.jpeg', '.png')):

					image_folder_path = root
					print(image_folder_path,'-----------imagepath')


					# check this is not reject folder

					images2json(image_folder_path, subscription_key, url,rerun=rerun )


	

## detect underlines 
def detect_underline(input_image_path, output_image_path, min_length=50, max_length=1000, save=False):
	'''
	Detects underlines from the input image and returns underline coodinates and underlined output images
	Params: 
	:input_image_path: Path to input image
	:output_image_path: Path to output underlined image
	:min_length: min length of the underlines to be detected
	:max_length: max length of the underlines to be detected
	:save: If save is true then underlined image is saved in the output path

	Parameter to tweek : lowerthres and higher thres for cv2.Canny 
						minlinelength and maxlinegap for cv2.HoughlinesP (this different from min_length and max_length)


	'''
	try:
		image = cv2.imread(input_image_path)
		gray_scale=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)

		th1,img_bin = cv2.threshold(gray_scale,150,225,cv2.THRESH_BINARY)
		
		lWidth = 3
		lineMinWidth = 30
		
		kernal1 = np.ones((lWidth,lWidth), np.uint8)
		kernal1h = np.ones((1,lWidth), np.uint8)
	#     kernal1v = np.ones((lWidth,1), np.uint8)

		kernal6 = np.ones((lineMinWidth,lineMinWidth), np.uint8)
		kernal6h = np.ones((1,lineMinWidth), np.uint8)
	#     kernal6v = np.ones((lineMinWidth,1), np.uint8)

		# detect horizontal lines
		img_bin_h = cv2.morphologyEx(~img_bin, cv2.MORPH_CLOSE, kernal1h) # bridge small gap in horizonntal lines

		img_bin_h = cv2.morphologyEx(img_bin_h, cv2.MORPH_OPEN, kernal6h) # kep ony horiz lines by eroding everything else in hor direction
		img_bin_final = fix(img_bin_h)

		edges = cv2.Canny(img_bin_final,150,255,3) # image, lower_thres, higher_thres , aperture_size
		minlinelength = 100  
		maxlinegap= 5
		lines = cv2.HoughLinesP(edges,1,np.pi/180,100,minlinelength,maxlinegap)  
		coordinates=[]
		line_length = []
		if len(lines)>0:
			for k in lines:
				for x1,y1,x2,y2 in k:
					dist= math.sqrt((x2-x1)**2 + (y2-y1)**2)
					if dist > min_length and dist < max_length:
						cv2.line(image, (x1,y1),(x2,y2),(0,255,0),2)

						# save coordinates and line length
						coordinates.append([x1,y1,x2,y2])
						line_length.append(dist)
		if save is True:        
			cv2.imwrite(output_image_path, image)
		print("Num of underlines: ",len(coordinates))

	except Exception as e:
		print("lines detection failed with : "+input_image_path)
		coordinates=[]
		line_length=[]

	return coordinates,line_length

def fix(img):
	img[img>127]=255
	img[img<127]=0
	return img
	

def dir_to_underlines(input_dir, min_length=50, max_length=1000, rewrite= False):
	'''
	Detects underlines in images from the input_dir (except reject_folder)
	:input_dir: path to input directory
	:min_length: input to detect_underline()
	:max_length: input to detect_underline()
	:rewrite: If True ,rewrite the existing output 
	Note: make sure json file from Azure output is present otherwise it wont save the coordinates output
	
	'''
	directory= MAKETREEDIR()
	for root, dirname, files in os.walk(input_dir):
		if not root.endswith(('reject_folder','underlined_images','output_underline','output_checkbox','checkbox_images', 'output_tables')) :
			if len(files) > 0:
				if files[0].endswith(('.jpg', '.jpeg', '.png')):
					
					underlined_dir= os.path.join(root,'underlined_images')
					directory.makedir(underlined_dir)
					output_underline = os.path.join(root,'output_underline')
					directory.makedir(output_underline)
					for item in files:
						print(item)
						input_image_path= str(os.path.join(root,item))
						output_image_path = str(os.path.join(underlined_dir,item))
						# _json_path = str(os.path.join(os.path.join(root,'output'),item[:-4]+'.json'))
						output_underline_path = str(os.path.join(output_underline, item[:-4]+'.json'))
						print(output_underline_path)
						
						my_file = Path(output_underline_path)
						condition = rewrite if Path.is_file() else True
						if condition:
							file = open(output_underline_path, 'w')
							coord, line_length=detect_underline(input_image_path,output_image_path,min_length=min_length,max_length=max_length,save=True)
						# make a list of dictionaries 
							dict_list = []
							for i,j in enumerate(coord):
								dict_list.append({'endpoints':str(j), 'length':str(line_length[i])})
							data = {"underlines":dict_list}
							print(data)
							# print(output_underline_path	)
						# print(dict_list)
						# print("loading json -----------------------------------------")
						# js = json.load(file)
						# print(js)
						# js["recognitionResult"]["underlines"] = dict_list
						# print(js)
							file.write(json.dumps(data))
							# js = json.dumps(data)
							# file.write(js)
						# json.dump(js, file, encoding='UTF-8')
							file.close()
							




if __name__=='__main__':
	# dir_to_image()
	img = cv2.imread("/home/sandipan/bridgei2i/output.jpg")
	azure_api(img, os.environ['COMPUTER_VISION_SUBSCRIPTION_KEY'], os.environ['COMPUTER_VISION_ENDPOINT'])
	images2json("/home/sandipan/bridgei2i/images_morella_new/", os.environ['COMPUTER_VISION_SUBSCRIPTION_KEY'], os.environ['COMPUTER_VISION_ENDPOINT'])
	# dir_to_json()
	# dir_to_underlines("/home/sandipan/bridgei2i/Submissions_images_json/", min_length=50,max_length=1000)
	# then run checkbox_opencv.py
	# for table extraction run 
