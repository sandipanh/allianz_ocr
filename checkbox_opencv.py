

import numpy as np

import cv2
import os
import time
import requests
import json
import logging
from pathlib import Path
import re

import shutil
from pdf2image import convert_from_path
from MakeTreeDir import MAKETREEDIR
from PyPDF2 import PdfFileReader
import pdb
# import imutils
# pdb.set_trace()
def experimental_checkbox_detection():
	image_path='./20 Sienna Corporate Services - PHF Sergio Steinberg_2.jpg'
	image=cv2.imread(image_path)
	gray_scale=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
	image1=image.copy()
	th1,img_bin = cv2.threshold(gray_scale,150,225,cv2.THRESH_BINARY)

	cv2.imwrite('0_binary.jpg',img_bin)
	cv2.namedWindow("0_binary",cv2.WINDOW_NORMAL)
	cv2.imshow("0_binary",img_bin)
	cv2.waitKey(0)


	lWidth = 3
	lineMinWidth = 30

	kernal1 = np.ones((lWidth,lWidth), np.uint8)
	kernal1h = np.ones((1,lWidth), np.uint8)
	kernal1v = np.ones((lWidth,1), np.uint8)

	kernal6 = np.ones((lineMinWidth,lineMinWidth), np.uint8)
	kernal6h = np.ones((1,lineMinWidth), np.uint8)
	kernal6v = np.ones((lineMinWidth,1), np.uint8)




	# detect horizontal lines
	img_bin_h = cv2.morphologyEx(~img_bin, cv2.MORPH_CLOSE, kernal1h) # bridge small gap in horizontal lines

	img_bin_h = cv2.morphologyEx(img_bin_h, cv2.MORPH_OPEN, kernal6h) # keep ony horiz lines by eroding everything else in hor direction


	# detect vertical lines
	img_bin_v = cv2.morphologyEx(~img_bin, cv2.MORPH_CLOSE, kernal1v)  # bridge small gap in vert lines
	img_bin_v = cv2.morphologyEx(img_bin_v, cv2.MORPH_OPEN, kernal6v)	 # keep ony vert lines by eroding everything else in vert direction

	img_bin_final = fix(fix(img_bin_h)|fix(img_bin_v))
	finalKernel = np.ones((5,5), np.uint8)
	img_bin_final=cv2.dilate(img_bin_final,finalKernel,iterations=1)


	ret, labels, stats,centroids = cv2.connectedComponentsWithStats(~img_bin_final, connectivity=8, ltype=cv2.CV_32S)

	c=0
	for x,y,w,h,area in stats[2:]:
		# print(x,y,w,h,area)
		c=c+1
		filled = False
		if area < 1000:
			# print(x,y,x+w,y+h, '-******************************')
			cv2.rectangle(image,(x,y),(x+w,y+h),(255,0,0),2)
			x1= int(x+w/2-3)
			x2=int(x+w/2+3)
			y1=int(y+h/2-3)
			y2=int(y+h/2+3)
			rect1 = img_bin[y1 :y2, x1:x2].copy()
			cv2.rectangle(image,(x1,y1),(x2,y2),(0,255,0),1)
			mean = rect1.mean()
			if mean > 200:
				filled = True
			print(mean)
			# box = image[x:x+w+2, y:y+h+2]
			# gray = cv2.cvtColor(box, cv2.COLOR_BGR2GRAY)
			# _,th1 = cv2.threshold(gray,127,255,cv2.THRESH_BINARY_INV)
			# th1 = th1/255
			# print(sum(sum(th1)))
			
			

				


			# print(checkbox)
			
			# find countours inside the rectangle whose area is above a threshold 
			# cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
			
			# gray = cv2.cvtColor(box, cv2.COLOR_BGR2GRAY)
			# edged = cv2.Canny(checkbox, 75, 200)
			# im2,contours = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)


			# ret,bw = cv2.threshold(gray,220,255,cv2.THRESH_BINARY_INV)
			# _,contours,hierarchy = cv2.findContours(bw, cv2.RETR_CCOMP,1)
			# _,contours= cv2.findContours(box, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
			print( "-------------------------")

			# cntLen=10
			# for ct in contours:
			# 	if len(contours) > cntLen:
			# 		cv2.drawContours(box,[ct],0,(255,0,0),2)
			# cnts = imutils.grab_contours(cnts)
			# if len(cnts) > 0:
			# 	print(len(cnts))
			# cv2.namedWindow('checkbox', cv2.WINDOW_NORMAL)
			# cv2.imshow("checkbox", rect1)
			# cv2.waitKey(0)
			# cv2.destroyAllWindows()

		

	# cv2.imwrite('output1.jpg',image)
	cv2.namedWindow('output', cv2.WINDOW_NORMAL)
	cv2.imshow("output", image)
	cv2.waitKey(0)
	cv2.destroyAllWindows()


	# find circles
	circles = cv2.HoughCircles(image=img_bin, method=cv2.HOUGH_GRADIENT, dp=1, 
										minDist=5, param1=90, param2=39, maxRadius=80)

	for co, i in enumerate(circles[0, :], start=1):
		# draw the outer circle in green
		area = np.pi * i[2]**2
		print(area,"-------------------------------------------------------")
		if area > 1000 and area < 1500:
			cv2.circle(image,(i[0],i[1]),int(i[2]),(0,0,255),3)
			# draw the center of the circle in red
			# cv2.circle(image,(i[0],i[1]),2,(0,255,0),3)
			x1 = int(i[0]) + int(i[2]/10) -3
			x2= int(i[0]) + int(i[2]/10) +3 
			y1 = int(i[1]) + int(i[2]/10) -3 
			y2 =int(i[1]) + int(i[2]/10) +3 

		## y: y+h, x: x+w and not x:x+h, y,y+h 
		## make a copy() other it modifies the original image
		# if u miss these 2 points u may spent a day debugging :( 
			rect2 = img_bin[y1:y2, x1:x2].copy()
			cv2.rectangle(image,(x1,y1),(x2,y2),(0,255,0),1)
			mean2 = rect2.mean()
		# print(mean2, "-------mean2", type(rect2))


	cv2.namedWindow('including circles', cv2.WINDOW_NORMAL)
	cv2.imshow("including circles", image)
	cv2.waitKey(0)
	cv2.destroyAllWindows()


## TODO refine parameters of circle detection

'''
{"checkboxes": [{"checbox_coordinate1": [21,45,21,2], "filled": True, },{},{},{}]}, {radio}

'''
def fix(img):
	img[img>127]=255
	img[img<127]=0
	return img
def detect_checkbox_radio(input_image_path, output_image_path, thres_mean=100,save=True):
	'''
	:input_image_path: Path to input image
	:output_image_path: Path to output image
	:thres_mean: this is the threshold for detecting filled/ non-filled checkbox/ radio buttons 
	Output:  1. outputs coordinates of the detected checkboxes and whether it is filled or not . 	
				2. Outputs center and radius of the detected radio buttons and whether it is filled or not


	Some links to help with deciding correct parameters for HoughCircles
	
	[https://stackoverflow.com/questions/10716464/what-are-the-correct-usage-parameter-values-for-houghcircles-in-opencv-for-iris] 
	[https://stackoverflow.com/questions/9860667/writing-robust-color-and-size-invariant-circle-detection-with-opencv-based-on]


	'''
	try:
		image=cv2.imread(input_image_path)
		gray_scale=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
		image1=image.copy()
		th1,img_bin = cv2.threshold(gray_scale,150,225,cv2.THRESH_BINARY)


		lWidth = 3
		lineMinWidth = 30

		kernal1 = np.ones((lWidth,lWidth), np.uint8)
		kernal1h = np.ones((1,lWidth), np.uint8)
		kernal1v = np.ones((lWidth,1), np.uint8)

		kernal6 = np.ones((lineMinWidth,lineMinWidth), np.uint8)
		kernal6h = np.ones((1,lineMinWidth), np.uint8)
		kernal6v = np.ones((lineMinWidth,1), np.uint8)


		# detect horizontal lines
		img_bin_h = cv2.morphologyEx(~img_bin, cv2.MORPH_CLOSE, kernal1h) # bridge small gap in horizontal lines

		img_bin_h = cv2.morphologyEx(img_bin_h, cv2.MORPH_OPEN, kernal6h) # keep ony horiz lines by eroding everything else in hor direction


		# detect vertical lines
		img_bin_v = cv2.morphologyEx(~img_bin, cv2.MORPH_CLOSE, kernal1v)  # bridge small gap in vert lines
		img_bin_v = cv2.morphologyEx(img_bin_v, cv2.MORPH_OPEN, kernal6v)	 # keep ony vert lines by eroding everything else in vert direction


		img_bin_final = fix(fix(img_bin_h)|fix(img_bin_v))
		finalKernel = np.ones((5,5), np.uint8)
		img_bin_final=cv2.dilate(img_bin_final,finalKernel,iterations=1)


		ret, labels, stats,centroids = cv2.connectedComponentsWithStats(~img_bin_final, connectivity=8, ltype=cv2.CV_32S)
	except Exception as e:
		print(e)
		print("Issue with image . Can't start checkbox detection")
		return [],[]

	checkbox_data = []
	try:
		for x,y,w,h,area_box in stats[2:]:
			# print(x,y,w,h,area)
		
			filled_checkbox= False
			if area_box < 1000:
			# print(x,y,x+w,y+h, '-******************************')
				cv2.rectangle(image,(x,y),(x+w,y+h),(255,0,0),2)
				x1= int(x+w/2-3)
				x2=int(x+w/2+3)
				y1=int(y+h/2-3)
				y2=int(y+h/2+3)

				## y: y+h, x: x+w and not x:x+h, y,y+h 
				## make a copy() other it modifies the original image
				# if u miss these 2 points u may spend a day debugging :( 

				rect1 = img_bin[y1 :y2, x1:x2].copy()
				cv2.rectangle(image,(x1,y1),(x2,y2),(0,255,0),1)
				mean1 = rect1.mean()
				if mean1 <thres_mean:
					filled_checkbox = True
				# print(mean)
				checkbox_data.append({"coordinates":str([x1,y1,x2,y2]),"filled":str(filled_checkbox)})
	except Exception as e :
		print(e)
		checkbox_data = []

	try:
		# find circles
		circles = cv2.HoughCircles(image=img_bin, method=cv2.HOUGH_GRADIENT, dp=1,minDist=5, param1=90, param2=39, maxRadius=80)
		filled_circle = False
		radio_data = []
		for co, i in enumerate(circles[0, :], start=1):
			# draw the outer circle in green
			area_circle = np.pi * i[2]**2
			# print(area)
			if area_circle > 1000 and area_circle< 1500:
				cv2.circle(image,(i[0],i[1]),int(i[2]),(0,0,255),3)
				# draw the center of the circle in red
				# cv2.circle(image,(i[0],i[1]),2,(0,255,0),3)
				cx1 = int(i[0]) + int(i[2]/10) -3
				cx2= int(i[0]) + int(i[2]/10) +3 
				cy1 = int(i[1]) + int(i[2]/10) -3 
				cy2 =int(i[1]) + int(i[2]/10) +3 

				rect2 = img_bin[cy1:cy2, cx1:cx2].copy()
				cv2.rectangle(image,(cx1,cy1),(cx2,cy2),(0,255,0),1)
				mean2 = rect2.mean()
				if mean2 < thres_mean:
					filled_circle = True
				radio_data.append({"center":str([i[0],i[1]]),"radius":str(i[2]), "filled":str(filled_circle)})
	except Exception as e:
		print(e)
		radio_data = []

				
				
		if save is True:
			cv2.imwrite(output_image_path,image)
					
	
	
	
	return checkbox_data,radio_data



			
def dir_to_checkbox(input_dir, rewrite=False):

	directory= MAKETREEDIR()
	for root, dirname, files in os.walk(input_dir):
		if not root.endswith(('reject_folder','underlined_images','output_underline','output_checkbox','checkbox_images')) :
			if len(files) > 0:
				if files[0].endswith(('.jpg', '.jpeg', '.png')):
					checkbox_dir = os.path.join(root,'checkbox_images')
					directory.makedir(checkbox_dir)
					output_checkbox = os.path.join(root,'output_checkbox')
					directory.makedir(output_checkbox)

					for item in files:
						print(item)
						input_image_path =  str(os.path.join(root,item))
						output_image_path = str(os.path.join(checkbox_dir,item))
						output_checkbox_path = str(os.path.join(output_checkbox, item[:-4]+'.json'))
						print(output_checkbox_path)

						my_file = Path(output_checkbox_path)
						condition = rewrite if my_file.is_file() else True  
						if condition:
							file = open(output_checkbox_path,'w' )
							checkbox_data,radio_data = detect_checkbox_radio(input_image_path, output_image_path, thres_mean=100, save=True)
							data = {"checkbox":checkbox_data, "radio": radio_data}

							print(data)
							file.write(json.dumps(data))
							file.close()



dir_to_checkbox("/home/sandipan/bridgei2i/Submissions_images_json/", True)

# experimental_checkbox_detection()