# README #

### Scope
The objective of this project is to extract information from pdf files and output it in a structured output format such that it can be used to make NLP based decisions.

Important steps include : 
1. Converting pdf to images
2. Run Azure ocr on images and output json
3. underline detection from images and output json and underlined images 
4. Checkbox and radio button detection and output json and checbox images 
5. Table extraction from pdfs and output xls files for each pdf.


### Installation
conda env create -f environment.yml
conda activate bridgei2i
pip install torch==1.3.1 torchvision==0.4.2 


If some function fails to run with the above lines, then run
pip install -r packages.txt
### What is this repository for? ###

* Table detection
* Checkbox detection
* Azure ocr
* Underline detection




### How do I get set up? ###
for **Azure ocr**
 
#### Linux 
add the following lines in ~/.bashrc
export COMPUTER_VISION_SUBSCRIPTION_KEY=<subscription key>
export COMPUTER_VISION_ENDPOINT=<endpoint-key>
#### Windows
Add COMPUTER_VISION_SUBSCRIPTION_KEY and COMPUTER_VISION_ENDPOINT to your environment variables 



### Contribution guidelines ###
will be added later


### Who do I talk to? ###

sandipan.haldar@bridgei2i.com




1. Scanned pdf with printed  or digital

convert scanned pdf --> digital pdf
   

Install ocrmypdf library for the conversion (does not work on handwritten pdf)
Use the following command
ocrmypdf <input-pdf> <output-pdf>


use pdf parsers- like pdftotree 

table extraction  - camelot + tiny yolo architecture (converted pdf + pg number ) -> xls sheet table 

2. Handwriiten pdf (combination printed and handwritten)

convert the pdf into a set of images 
pdf2image (bridgei2i_ocr) inbuild function 
for each run ocr engine (tesseract, azure ocr) --> json (for each word and for line --- text , bounding box)
key-value extraction  --> key if present  -> sentence  


table extraction : 
pipeline - > run azure_ocr -> map json output to pdf in **pdf space** using pypdf2 --> camolot+ yolo steps 

# refernce for underline text detection 
underline  : x_1 y1 x_2 y2 
mean_y (y1+y2)/2

bounding boxes : x1 y1 x2 y2 x3 y3 x4 y4 

mean_text = (y1+y2)/2

seach for boxes whose mean_text is within some threshold of mean_y
box 1 box2 box3 
check which box's x1 or  x3 lies withing x_1 and x_2 : result 2 boxes 